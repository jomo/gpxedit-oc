msgid ""
msgstr ""
"Project-Id-Version: gpxedit\n"
"Report-Msgid-Bugs-To: translations\\@example.com\n"
"POT-Creation-Date: 2020-02-16 13:46+0100\n"
"PO-Revision-Date: 2020-02-16 13:12\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Spanish\n"
"Language: es_ES\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Crowdin-Project: gpxedit\n"
"X-Crowdin-Language: es-ES\n"
"X-Crowdin-File: /master/translationfiles/templates/gpxedit.pot\n"

#: /var/www/html/n18/apps/gpxedit/appinfo/app.php:43
#: /var/www/html/n18/apps/gpxedit/specialAppInfoFakeDummyForL10nScript.php:2
#: /var/www/html/n18/apps/gpxedit/templates/admin.php:7
msgid "GpxEdit"
msgstr "GpxEdit"

#: /var/www/html/n18/apps/gpxedit/js/admin.js:15
#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:403
#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1301
#: /var/www/html/n18/apps/gpxedit/templates/admin.php:27
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:165
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:209
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:255
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:305
msgid "Delete"
msgstr "Eliminar"

#: /var/www/html/n18/apps/gpxedit/js/filetypes.js:11
msgid "Load in GpxEdit"
msgstr "Carga en GpxEdit"

#: /var/www/html/n18/apps/gpxedit/js/filetypes.js:36
msgid "Edit with GpxEdit"
msgstr "Editar con GpxEdit"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:399
msgid "Draw a track"
msgstr "Dibujar una trayectoria"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:400
msgid "Add a waypoint"
msgstr "Agregar punto de camino"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:401
msgid "Edit"
msgstr "Editar"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:402
msgid "Nothing to edit"
msgstr "Nada para editar"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:404
msgid "Nothing to delete"
msgstr "Nada para eliminar"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:405
msgid "Validate changes"
msgstr "Validar cambios"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:406
msgid "Ok"
msgstr "Aceptar"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:407
msgid "Discard all changes"
msgstr "Descartar todos los cambios"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:408
#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:416
msgid "Cancel"
msgstr "Cancelar"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:409
msgid "Drag to move elements,<br/>click to remove a point<br/>hover a middle marker and press \"Del\" to cut the line"
msgstr "Arrastrar para mover elementos, <br/> haga clic para quitar un punto de <br/> pase un marcador central y presione \"Del\" para la línea de corte"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:410
msgid "Click cancel to undo changes"
msgstr "Haz clic para deshacer los cambios"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:411
msgid "Click on an element to delete it"
msgstr "Haga clic en un elemento para seleccionarlo"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:412
msgid "Click map to add waypoint"
msgstr "Haz clic en mapa para añadir punto de camino"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:413
msgid "Click to start drawing track"
msgstr "Haz clic para empezar a dibujar la trayectoria"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:414
msgid "Click to continue drawing track"
msgstr "Haz clic para continuar dibujando la trayectoria"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:415
msgid "Click last point to finish track"
msgstr "Haz clic en el último punto para finalizar la trayectoria"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:417
msgid "Cancel drawing"
msgstr "Cancelar dibujo"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:418
msgid "Finish"
msgstr "Finalizar"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:419
msgid "Finish drawing"
msgstr "Terminar dibujo"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:420
msgid "Delete last point"
msgstr "Borrar el último punto"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:421
msgid "Delete last drawn point"
msgstr ""

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:488
msgid "Track"
msgstr "Trayectoria"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:493
msgid "Route"
msgstr "Trayecto"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:498
msgid "Waypoint"
msgstr "Punto de camino"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:503
msgid "Name"
msgstr "Nombre"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:504
msgid "Description"
msgstr "Descripción"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:505
msgid "Comment"
msgstr "Comentario"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:506
msgid "Link text"
msgstr "Texto de enlace"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:508
msgid "Link URL"
msgstr "URL del Enlace"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:511
msgid "Lat"
msgstr "Latitud"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:513
msgid "Lon"
msgstr "Longitud"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:515
msgid "Symbol"
msgstr "Símbolo"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:518
msgid "No symbol"
msgstr "Ningún símbolo"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:520
msgid "Unknown symbol"
msgstr "Símbolo desconocido"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1007
msgid "Failed to save file"
msgstr "Error al guardar archivo"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1025
msgid "File successfully saved as"
msgstr "Archivo guardado con éxito como"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1136
msgid "Impossible to load this file. "
msgstr "Imposible cargar este archivo. "

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1137
msgid "Supported formats are gpx, kml, csv (unicsv) and jpg."
msgstr "Formatos admitidos son gpx, kml, csv (unicsv) y jpg."

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1138
msgid "Load error"
msgstr "Error al cargar"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1241
msgid "Tile server \"{ts}\" has been deleted"
msgstr "Servidor de mosaico \"{ts}\" se ha eliminado"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1244
#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1248
msgid "Failed to delete tile server \"{ts}\""
msgstr "No se pudo eliminar el servidor de mosaico \"{ts}\""

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1263
msgid "Server name or server url should not be empty"
msgstr "Nombre del servidor o dirección del servidor no debe estar vacío"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1264
#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1269
msgid "Impossible to add tile server"
msgstr "Imposible añadir servidor de mosaico"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1268
msgid "A server with this name already exists"
msgstr "Un servidor con este nombre ya existe"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1334
msgid "Tile server \"{ts}\" has been added"
msgstr "Se ha agregado el servidor de mosaico \"{ts}\""

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1337
#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1341
msgid "Failed to add tile server \"{ts}\""
msgstr "No se pudo agregar el servidor de mosaico \"{ts}\""

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1413
msgid "Failed to restore options values"
msgstr "No se pudo guardar valores de opciones"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1414
#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1469
msgid "Error"
msgstr "Error"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1468
msgid "Failed to save options values"
msgstr "No se pudo guardar valores de opciones"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1535
#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1542
#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1549
msgid "Impossible to write file"
msgstr "Imposible escribir este archivo"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1536
msgid "write access denied"
msgstr "acceso de escritura denegado"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1543
msgid "folder does not exist"
msgstr "la carpeta no existe"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1550
msgid "folder write access denied"
msgstr "carpeta acceso de escritura denegado"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1556
msgid "Bad file name, must end with \".gpx\""
msgstr "Nombre de archivo incorrecto, debe terminar con \".gpx\""

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1672
msgid "Load file (gpx, kml, csv, png)"
msgstr "Cargar archivo (gpx, kml, csv, png)"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1689
msgid "Load folder"
msgstr "Cargar carpeta"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1701
msgid "There is nothing to save"
msgstr "No hay nada para guardar"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1706
msgid "Where to save"
msgstr "Donde guardar"

#: /var/www/html/n18/apps/gpxedit/js/leaflet.js:5
msgid "left"
msgstr "izquierda"

#: /var/www/html/n18/apps/gpxedit/js/leaflet.js:5
msgid "right"
msgstr "derecha"

#: /var/www/html/n18/apps/gpxedit/specialAppInfoFakeDummyForL10nScript.php:3
msgid " "
msgstr ""

#: /var/www/html/n18/apps/gpxedit/templates/admin.php:8
msgid "Extra symbols"
msgstr "Símbolos adicionales"

#: /var/www/html/n18/apps/gpxedit/templates/admin.php:9
msgid "Those symbols will be available in GpxEdit."
msgstr "Esos símbolos estarán disponibles en GpxEdit."

#: /var/www/html/n18/apps/gpxedit/templates/admin.php:10
msgid "Keep in mind that only symbol names are saved in gpx files. Other programs will display default symbol if they do not know a symbol name."
msgstr "Toma en cuenta que solo los nombres de los símbolos son guardados en los archivos gpx. Otros programas mostraran los símbolos por defectos si ellos no saben el nombre del símbolo."

#: /var/www/html/n18/apps/gpxedit/templates/admin.php:34
msgid "Recommended image ratio : 1:1"
msgstr "Proporción de imagen recomendada: 1:1"

#: /var/www/html/n18/apps/gpxedit/templates/admin.php:35
msgid "Recommended image resolution : between 24x24 and 50x50"
msgstr "Resolución de imagen recomendada: entre 24x24 y 50x50"

#: /var/www/html/n18/apps/gpxedit/templates/admin.php:36
msgid "Accepted image format : png"
msgstr "Formato de imagen aceptado: png"

#: /var/www/html/n18/apps/gpxedit/templates/admin.php:38
msgid "New symbol name"
msgstr "Nuevo nombre de símbolo"

#: /var/www/html/n18/apps/gpxedit/templates/admin.php:42
msgid "Upload new symbol image"
msgstr "Subir nueva imagen de símbolo"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:4
msgid "Load and save files"
msgstr "Cargar y guardar archivos"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:5
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:106
msgid "Options"
msgstr "Opciones"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:6
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:316
msgid "About GpxEdit"
msgstr "Acerca de GpxEdit"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:24
msgid "Load file"
msgstr "Cargar archivo"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:25
msgid "Load directory"
msgstr "Directorio de carga"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:27
msgid "all files"
msgstr "todos los archivos"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:35
msgid "Save"
msgstr "Guardar"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:37
msgid "File name"
msgstr "Nombre del archivo"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:40
msgid "Metadata"
msgstr "Metadata"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:42
msgid "Track name (optional)"
msgstr "Nombre de trayectoria (opcional)"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:44
msgid "Description (optional)"
msgstr "Descripción (opcional)"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:46
msgid "Link text (optional)"
msgstr "Texto del Enlace (opcional)"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:48
msgid "Link URL (optional)"
msgstr "URL del Enlace (opcional)"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:51
msgid "Choose directory and save"
msgstr "Elegir directorio y guardar"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:54
msgid "Clear map"
msgstr "Limpiar mapa"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:65
msgid "loading file"
msgstr "cargando archivo"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:69
msgid "exporting file to gpx"
msgstr "exportar archivo a gpx"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:73
msgid "saving file"
msgstr "guardando Archivo"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:109
msgid "Default symbol for waypoints when value is not set"
msgstr "Símbolo por defecto de puntos de camino cuando el valor no está establecido"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:110
msgid "Waypoint style"
msgstr "Estilos de punto de camino"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:114
msgid "Tooltip"
msgstr "ToolTip"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:116
msgid "on hover"
msgstr "en suspenso"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:117
msgid "permanent"
msgstr "permanente"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:119
msgid "Units (needs page reload to take effect)"
msgstr "Unidades (necesita página recarga surta efecto)"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:121
msgid "Metric"
msgstr "Métrico"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:122
msgid "English"
msgstr "Inglés"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:123
msgid "Nautical"
msgstr "Náutica"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:125
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:127
msgid "Use defined symbols instead of default symbol"
msgstr "Utilizar símbolos definidos en lugar de símbolo por defecto"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:126
msgid "Use defined symbols"
msgstr "Utilizar símbolos definidos"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:129
msgid "Clear map before loading"
msgstr "Limpiar mapa antes de cargar"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:131
msgid "Approximate new points elevations"
msgstr "Aproximar nuevos puntos de elevación"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:135
msgid "Custom tile servers"
msgstr "Servidores de mosaicos personalizados"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:138
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:178
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:222
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:268
msgid "Server name"
msgstr "Nombre del servidor"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:139
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:179
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:223
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:269
msgid "For example : my custom server"
msgstr "Por ejemplo : mi servidor personal"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:140
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:180
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:224
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:270
msgid "Server url"
msgstr "Url del servidor"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:141
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:225
msgid "For example : http://tile.server.org/cycle/{z}/{x}/{y}.png"
msgstr "Por ejemplo: http://tile.server.org/cycle/{z}/{x}/{y}.png"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:142
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:182
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:226
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:272
msgid "Min zoom (1-20)"
msgstr "Zoom mínimo (1-20)"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:144
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:184
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:228
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:274
msgid "Max zoom (1-20)"
msgstr "Zoom máximo (1-20)"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:146
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:190
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:236
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:286
msgid "Add"
msgstr "Añadir"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:149
msgid "Your tile servers"
msgstr "Tus servidores de mosaico"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:175
msgid "Custom overlay tile servers"
msgstr "Servidores de mosaicos de superposición personales"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:181
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:271
msgid "For example : http://overlay.server.org/cycle/{z}/{x}/{y}.png"
msgstr "Por ejemplo: http://overlay.server.org/cycle/{z}/{x}/{y}.png"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:186
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:276
msgid "Transparent"
msgstr "Transparente"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:188
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:278
msgid "Opacity (0.0-1.0)"
msgstr "Opacidad (0.0-1.0)"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:193
msgid "Your overlay tile servers"
msgstr "Sus servidores de mosaico de superposición"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:219
msgid "Custom WMS tile servers"
msgstr "Servidores de mosaico WMS personalizados"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:230
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:280
msgid "Format"
msgstr "Formato"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:232
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:282
msgid "WMS version"
msgstr "Versión de WMS"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:234
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:284
msgid "Layers to display"
msgstr "Capas para mostrar"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:239
msgid "Your WMS tile servers"
msgstr "Sus servidores de mosaico WMS"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:265
msgid "Custom WMS overlay servers"
msgstr "Servidores de azulejo WMS de superposición personalizados"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:289
msgid "Your WMS overlay tile servers"
msgstr "Sus servidores de mosaico de superposición WMS"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:318
msgid "Features overview"
msgstr "Resumen de características"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:320
msgid "Draw, edition and deletion buttons are in the map's bottom-left corner."
msgstr "Botones de dibujo, edición y eliminación están en la esquina inferior izquierda del mapa."

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:322
msgid "You can draw a line or add a marker."
msgstr "Puede dibujar una línea o agregar un marcador."

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:324
msgid "If you click on a line or a marker, a popup pops and let you set the object properties."
msgstr "Si haces clic en una línea o un marcador, una ventana emergente aparece y le permite establecer las propiedades del objeto."

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:329
msgid "After a click on \"edition\" button, in edition mode, you can"
msgstr "Después de un clic en el botón \"edición\", en el modo de edición, puedes"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:332
msgid "move markers"
msgstr "marcadores de movimiento"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:333
msgid "move line points"
msgstr "mover puntos de línea"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:334
msgid "click on a line point to remove it"
msgstr "haga clic en un punto de la línea para quitarlo"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:335
msgid "hover a \"middle marker\" (between two line points) and press \"Del\" to cut the line in two (this action cannot be canceled)"
msgstr "coloca el puntero en un \"marcador medio\" (entre dos puntos de línea) y presiona \"Del\" para cortar la línea en dos (esta acción no puede ser cancelada)"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:338
msgid "Shortcuts"
msgstr "Accesos rápidos"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:340
msgid "toggle sidebar"
msgstr "ocultar/mostrar barra lateral"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:341
msgid "toggle minimap"
msgstr "minimapa lateral"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:344
msgid "Documentation"
msgstr "Documentación"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:352
msgid "Source management"
msgstr "Administración de fuente"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:363
msgid "Authors"
msgstr "Autores"

